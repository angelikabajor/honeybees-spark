# Honeybees need your help!
A savvy swarm lived peacufully in some undisclosed location. One day, the queen said:

> _I'm fed up with this BS. You can't even tell me which pollen is the most effective for our swarm, or who is our **udarnik**!_

This made the bees sad (you can imagine [Der Untergang](http://en.wikipedia.org/wiki/Downfall_(2004_film)) final scene here)... However, not all was lost! One smart bee came forward and said:

> _Oh c'mon, it's no big deal. We've been tracking each of our bees and collected some really good data. I'm sure it could be used for the swarm analytics._

# Data

There are two CSV files.

### pollen.csv

It contains information about each pollen effectivenes - i.e. how much of the sugar can be extracted from each mg of pollen.

### harvest.csv

This file contains information about harvest done by each of the bees each day. I.e. how much of what pollen was harvested on a given day.

# Task

Help the queen with finding out a couple of answers:

1. Which pollen contributed the most sugar in total?
2. Which pollen was most popular among the bees?
3. Which day was the best day for harvesting? Which one was the worst? (A chart or table that shows total sugar per day would be great.)
4. Which bee was most effective (**udarnik**)? Which one was the least? Effectiveness is measured as average sugar harvested per each working day. Getting a table with each bee relative effectiveness is a plus.

# Rules

* Use your favorite development platform.
* Quality first. Make it smart and well organized.
* Solve any number of tasks (but the more the better).
* Create models for the input data first.
* Use any preferred storage (memory-based is OK).
* Unit tested (queen likes to have the top code).

# Output

```
1. Which pollen contributed the most sugar in total?
+---------+-----------+
|pollen_id|total_sugar|
+---------+-----------+
|        1|    43367.0|
+---------+-----------+

2. Which pollen was most popular among the bees?
+---------+----------+
|pollen_id|popularity|
+---------+----------+
|        1|       182|
+---------+----------+

3. Which day was the best day for harvesting? Which one was the worst? (A chart or table that shows total sugar per day would be great.

Most efficient day:
+----------+-----------+
|       day|total_sugar|
+----------+-----------+
|2013-05-26|    2055.50|
+----------+-----------+


Least efficient day:
+----------+-----------+
|       day|total_sugar|
+----------+-----------+
|2013-06-30|     133.90|
+----------+-----------+


Sugar by day:
+----------+-----------+
|       day|total_sugar|
+----------+-----------+
|2013-04-01|     647.20|
|2013-04-02|     589.90|
|2013-04-03|     759.30|
|2013-04-04|     522.10|
|2013-04-05|     791.40|
|2013-04-06|     658.60|
|2013-04-07|     478.10|
|2013-04-08|     504.40|
|2013-04-09|     514.50|
|2013-04-10|     421.30|
|2013-04-11|     989.80|
|2013-04-12|    1377.20|
|2013-04-13|     856.30|
|2013-04-14|     819.00|
|2013-04-15|    1154.20|
|2013-04-16|     610.60|
|2013-04-17|     959.00|
|2013-04-18|    1483.70|
|2013-04-19|    1034.60|
|2013-04-20|     507.40|
+----------+-----------+
only showing top 20 rows

4. Which bee was most effective (**udarnik**)? Which one was the least? Effectiveness is measured as average sugar harvested per each working day. Getting a table with each bee relative effectiveness is a plus.

Most effective bee:
+------+-----------+
|bee_id|total_sugar|
+------+-----------+
|     2|     219.11|
+------+-----------+


Least effective bee:
+------+-----------+
|bee_id|total_sugar|
+------+-----------+
|    11|      53.68|
+------+-----------+


Bees effectiveness:
+------+-----------+
|bee_id|total_sugar|
+------+-----------+
|     1|      85.17|
|     2|     219.11|
|     3|     105.69|
|     4|      90.14|
|     5|      93.54|
|     6|     129.29|
|     7|      57.30|
|     8|     172.31|
|     9|     119.23|
|    10|      72.81|
|    11|      53.68|
|    12|     121.29|
|    13|      97.85|
|    14|     152.91|
|    15|     204.29|
+------+-----------+
```
