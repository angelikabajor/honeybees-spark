ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.7"

lazy val root = (project in file("."))
  .settings(
    name := "honeybees-spark",
    idePackagePrefix := Some("com.gitlab.angelikabajor")
  )

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "3.2.0",
  "org.apache.spark" %% "spark-sql" % "3.2.0",
  "org.scalatest" %% "scalatest" % "3.2.10" % Test,
  "org.apache.spark" %% "spark-core" % "3.2.0" % Test classifier "tests",
  "org.apache.spark" %% "spark-sql" % "3.2.0" % Test classifier "tests",
  "org.apache.spark" %% "spark-catalyst" % "3.2.0" % Test classifier "tests",
)
