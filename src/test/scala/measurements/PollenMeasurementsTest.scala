package com.gitlab.angelikabajor
package measurements

import org.apache.spark.SparkFunSuite
import org.apache.spark.sql.QueryTest.checkAnswer
import org.apache.spark.sql.test.SharedSparkSession
import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.matchers.should.Matchers._

class PollenMeasurementsTest extends SparkFunSuite with SharedSparkSession {

  import testImplicits._

  val day1 = "2013-04-01"
  val day2 = "2013-04-02"
  val day3 = "2013-04-03"
  val day4 = "2013-04-04"
  val pollenId0 = 0
  val pollenId1 = 1
  val pollenId2 = 2

  val pollensWithIds01: Seq[(Int, String, Int)] = Seq((pollenId0, "Bluebell", 1), (pollenId1, "Rhododendron", 1))
  val pollensWithIds012: Seq[(Int, String, Int)] = pollensWithIds01 :+ (pollenId2, "Thyme", 1)

  test("should return most contributed pollen") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (10, day1, pollenId0, 1),
      (11, day2, pollenId0, 1),
      (12, day1, pollenId1, 5),
      (13, day2, pollenId1, 5))

    val mostContributedPollen = PollenMeasurements.getMostContributedPollen(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    checkAnswer(mostContributedPollen, Seq(Row(pollenId1, 10.0)))
  }

  test("should return multiple most contributed pollens") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (10, day1, pollenId0, 1),
      (11, day2, pollenId0, 1),
      (12, day1, pollenId1, 5),
      (13, day2, pollenId1, 5),
      (14, day1, pollenId2, 5),
      (15, day2, pollenId2, 5))

    val mostContributedPollen = PollenMeasurements.getMostContributedPollen(toPollenDF(pollensWithIds012), toHarvestDF(harvest))
    checkAnswer(mostContributedPollen, Seq(Row(pollenId1, 10.0), Row(pollenId2, 10.0)))
  }

  test("should return empty most contributed pollens list when harvest list is empty") {
    val emptyHarvest: Seq[(Int, String, Int, Double)] = Seq()

    val mostContributedPollen = PollenMeasurements.getMostContributedPollen(toPollenDF(pollensWithIds01), toHarvestDF(emptyHarvest))
    mostContributedPollen should be(empty)
  }

  test("should return empty most contributed pollens list when pollens list is empty") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq((10, day1, pollenId0, 1))
    val emptyPollens: Seq[(Int, String, Int)] = Seq()

    val mostContributedPollen = PollenMeasurements.getMostContributedPollen(toPollenDF(emptyPollens), toHarvestDF(harvest))
    mostContributedPollen should be(empty)
  }

  test("should return most contributed pollen when some pollen id is missing") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (10, day1, pollenId0, 1),
      (11, day2, pollenId0, 1),
      (12, day1, pollenId1, 5),
      (13, day2, pollenId1, 5),
      (14, day1, pollenId2, 5),
      (15, day2, pollenId2, 5))

    val mostContributedPollen = PollenMeasurements.getMostContributedPollen(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    checkAnswer(mostContributedPollen, Seq(Row(pollenId1, 10.0)))
  }

  test("should return most popular pollen") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (10, day1, pollenId0, 5),
      (11, day1, pollenId1, 1),
      (12, day2, pollenId1, 1))

    val mostPopularPollen = PollenMeasurements.getMostPopularPollen(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    checkAnswer(mostPopularPollen, Seq(Row(pollenId1, 2)))
  }

  test("should return multiple most popular pollens") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (10, day1, pollenId0, 5),
      (11, day1, pollenId1, 1),
      (12, day2, pollenId1, 1),
      (13, day1, pollenId2, 2),
      (14, day2, pollenId2, 2))

    val mostPopularPollen = PollenMeasurements.getMostPopularPollen(toPollenDF(pollensWithIds012), toHarvestDF(harvest))
    checkAnswer(mostPopularPollen, Seq(Row(pollenId1, 2), Row(pollenId2, 2)))
  }

  test("should return empty most popular pollen list when harvest list is empty") {
    val emptyHarvest: Seq[(Int, String, Int, Double)] = Seq()

    val mostPopularPollen = PollenMeasurements.getMostPopularPollen(toPollenDF(pollensWithIds01), toHarvestDF(emptyHarvest))
    mostPopularPollen should be(empty)
  }

  test("should return empty most popular pollen list when pollens list is empty") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq((10, day1, pollenId0, 5))
    val emptyPollens: Seq[(Int, String, Int)] = Seq()

    val mostPopularPollen = PollenMeasurements.getMostPopularPollen(toPollenDF(emptyPollens), toHarvestDF(harvest))
    mostPopularPollen should be(empty)
  }

  test("should return most popular pollen when some pollen id is missing") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (10, day1, pollenId0, 5),
      (11, day1, pollenId1, 1),
      (12, day2, pollenId1, 1),
      (13, day1, pollenId2, 2),
      (14, day2, pollenId2, 2))

    val mostPopularPollen = PollenMeasurements.getMostPopularPollen(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    checkAnswer(mostPopularPollen, Seq(Row(pollenId1, 2)))
  }

  def toHarvestDF(harvest: Seq[(Int, String, Int, Double)]): DataFrame = {
    harvest.toDF(beeIdColName, dayColName, pollenIdColName, miligramsHarvestedColName)
  }

  def toPollenDF(pollen: Seq[(Int, String, Int)]): DataFrame = {
    pollen.toDF(idColName, nameColName, sugarPerMgColName)
  }

}
