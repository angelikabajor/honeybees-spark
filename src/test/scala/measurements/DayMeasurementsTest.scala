package com.gitlab.angelikabajor
package measurements

import org.apache.spark.SparkFunSuite
import org.apache.spark.sql.QueryTest.checkAnswer
import org.apache.spark.sql.test.SharedSparkSession
import org.apache.spark.sql.{DataFrame, Row}

class DayMeasurementsTest extends SparkFunSuite with SharedSparkSession {

  import testImplicits._

  val day1 = "2013-04-01"
  val day2 = "2013-04-02"
  val day3 = "2013-04-03"
  val day4 = "2013-04-04"
  val pollenId0 = 0
  val pollenId1 = 1
  val pollenId2 = 2

  val pollensWithIds01: Seq[(Int, String, Int)] = Seq((pollenId0, "Bluebell", 1), (pollenId1, "Rhododendron", 1))
  val pollensWithIds012: Seq[(Int, String, Int)] = pollensWithIds01 :+ (pollenId2, "Thyme", 1)

  val harvestForDays12: Seq[(Int, String, Int, Double)] = Seq(
    (10, day1, pollenId0, 1),
    (11, day1, pollenId1, 4),
    (12, day2, pollenId0, 3),
    (13, day2, pollenId1, 6))
  val harvestForPollens012AndUnknown: Seq[(Int, String, Int, Double)] = harvestForDays12 ++ Seq(
    (14, day3, pollenId2, 100),
    (15, day3, pollenId2, 100))
  val harvestForDays1234: Seq[(Int, String, Int, Double)] = harvestForDays12 ++ Seq(
    (16, day3, pollenId0, 1),
    (17, day3, pollenId1, 4),
    (18, day4, pollenId0, 3),
    (19, day4, pollenId1, 6))

  test("should return sugar per day") {
    val sugarByDay = DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(harvestForDays1234))
    checkAnswer(sugarByDay, Seq(Row(day1, 5.0), Row(day2, 9.0), Row(day3, 5.0), Row(day4, 9.0)))
  }

  test("should return empty sugar per day list when harvest list is empty") {
    val emptyHarvest: Seq[(Int, String, Int, Double)] = Seq()

    val sugarByDay = DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(emptyHarvest))
    checkAnswer(sugarByDay, Nil)
  }

  test("should return empty sugar per day list when pollens list is empty") {
    val emptyPollens: Seq[(Int, String, Int)] = Seq()

    val sugarByDay = DayMeasurements.getSugarPerDay(toPollenDF(emptyPollens), toHarvestDF(harvestForDays12))
    checkAnswer(sugarByDay, Nil)
  }

  test("should return sugar per day ignoring unknown pollenId") {
    val sugarByDay = DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(harvestForPollens012AndUnknown))
    checkAnswer(sugarByDay, Seq(Row(day1, 5.0), Row(day2, 9.0)))
  }

  test("should return most efficient days") {
    val mostEfficientDays = DayMeasurements.getMostEfficientDays(DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(harvestForDays12)))
    checkAnswer(mostEfficientDays, Seq(Row(day2, 9.0)))
  }

  test("should return multiple most efficient days") {
    val mostEfficientDays = DayMeasurements.getMostEfficientDays(DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(harvestForDays1234)))
    checkAnswer(mostEfficientDays, Seq(Row(day2, 9.0), Row(day4, 9.0)))
  }

  test("should return empty most efficient days list when sugar by day is empty") {
    val emptySugarByDay: Seq[(String, Double)] = Seq.empty

    val mostEfficientDays = DayMeasurements.getMostEfficientDays(emptySugarByDay.toDF(dayColName, totalSugarColName))
    checkAnswer(mostEfficientDays, Nil)
  }

  test("should return least efficient days") {
    val leastEfficientDays = DayMeasurements.getLeastEfficientDays(DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(harvestForDays12)))
    checkAnswer(leastEfficientDays, Seq(Row(day1, 5.0)))
  }

  test("should return multiple least efficient days") {
    val leastEfficientDays = DayMeasurements.getLeastEfficientDays(DayMeasurements.getSugarPerDay(toPollenDF(pollensWithIds01), toHarvestDF(harvestForDays1234)))
    checkAnswer(leastEfficientDays, Seq(Row(day1, 5.0), Row(day3, 5.0)))
  }

  test("should return empty least efficient days list when sugar by day is empty") {
    val emptySugarByDay: Seq[(String, Double)] = Seq.empty

    val leastEfficientDays = DayMeasurements.getLeastEfficientDays(emptySugarByDay.toDF(dayColName, totalSugarColName))
    checkAnswer(leastEfficientDays, Nil)
  }

  def toHarvestDF(harvest: Seq[(Int, String, Int, Double)]): DataFrame = {
    harvest.toDF(beeIdColName, dayColName, pollenIdColName, miligramsHarvestedColName)
  }

  def toPollenDF(pollen: Seq[(Int, String, Int)]): DataFrame = {
    pollen.toDF(idColName, nameColName, sugarPerMgColName)
  }
}
