package com.gitlab.angelikabajor
package measurements

import org.apache.spark.SparkFunSuite
import org.apache.spark.sql.QueryTest.checkAnswer
import org.apache.spark.sql.test.SharedSparkSession
import org.apache.spark.sql.{DataFrame, Row}

class BeeMeasurementsTest extends SparkFunSuite with SharedSparkSession {

  import testImplicits._

  val day1 = "2013-04-01"
  val day2 = "2013-04-02"
  val day3 = "2013-04-03"
  val beeId5 = 5
  val beeId6 = 6
  val beeId7 = 7
  val pollenId0 = 0
  val pollenId1 = 1
  val pollenId2 = 2
  val pollensWithIds01: Seq[(Int, String, Int)] = Seq((pollenId0, "Bluebell", 1), (pollenId1, "Rhododendron", 1))
  val pollensWithIds012: Seq[(Int, String, Int)] = pollensWithIds01 :+ (pollenId2, "Thyme", 1)

  test("should return effectiveness per bee") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6))

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    checkAnswer(beesEffectiveness, Seq(Row(beeId5, 2.0), Row(beeId6, 5.0)))
  }

  test("should return empty bees effectiveness list when harvest list is empty") {
    val emptyHarvest: Seq[(Int, String, Int, Double)] = Seq()

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(emptyHarvest))
    checkAnswer(beesEffectiveness, Nil)
  }

  test("should return empty bees effectiveness list when pollens list is empty") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6))
    val emptyPollens: Seq[(Int, String, Int)] = Seq()

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(emptyPollens), toHarvestDF(harvest))
    checkAnswer(beesEffectiveness, Nil)
  }

  test("should return effectiveness per bee ignoring unknown pollenId") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6),
      (beeId5, day3, pollenId2, 5),
      (beeId6, day3, pollenId2, 8))

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    checkAnswer(beesEffectiveness, Seq(Row(beeId5, 2.0), Row(beeId6, 5.0)))
  }

  test("should return most effective bees") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6))

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    val mostEffectiveBees = BeeMeasurements.getMostEffectiveBees(beesEffectiveness)
    checkAnswer(mostEffectiveBees, Seq(Row(beeId6, 5.0)))
  }

  test("should return multiple most effective bees") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6),
      (beeId7, day1, pollenId0, 4),
      (beeId7, day2, pollenId1, 6))

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    val mostEffectiveBees = BeeMeasurements.getMostEffectiveBees(beesEffectiveness)
    checkAnswer(mostEffectiveBees, Seq(Row(beeId6, 5.0), Row(beeId7, 5.0)))
  }

  test("should return empty most effective bees list when bees effectiveness is empty") {
    val emptyBeesEffectiveness: Seq[(Int, Double)] = Seq.empty

    val mostEffectiveBees = BeeMeasurements.getMostEffectiveBees(emptyBeesEffectiveness.toDF(beeIdColName, totalSugarColName))
    checkAnswer(mostEffectiveBees, Nil)
  }

  test("should return least effective bees") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6))

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    val mostEffectiveBees = BeeMeasurements.getLeastEffectiveBees(beesEffectiveness)
    checkAnswer(mostEffectiveBees, Seq(Row(beeId5, 2.0)))
  }

  test("should return multiple least effective bees") {
    val harvest: Seq[(Int, String, Int, Double)] = Seq(
      (beeId5, day1, pollenId0, 1),
      (beeId5, day2, pollenId1, 3),
      (beeId6, day1, pollenId0, 4),
      (beeId6, day2, pollenId1, 6),
      (beeId7, day1, pollenId0, 1),
      (beeId7, day2, pollenId1, 3))

    val beesEffectiveness = BeeMeasurements.getBeesEffectiveness(toPollenDF(pollensWithIds01), toHarvestDF(harvest))
    val mostEffectiveBees = BeeMeasurements.getLeastEffectiveBees(beesEffectiveness)
    checkAnswer(mostEffectiveBees, Seq(Row(beeId5, 2.0), Row(beeId7, 2.0)))
  }

  test("should return empty least effective bees list when bees effectiveness is empty") {
    val emptyBeesEffectiveness: Seq[(Int, Double)] = Seq.empty

    val mostEffectiveBees = BeeMeasurements.getLeastEffectiveBees(emptyBeesEffectiveness.toDF(beeIdColName, totalSugarColName))
    checkAnswer(mostEffectiveBees, Nil)
  }

  def toHarvestDF(harvest: Seq[(Int, String, Int, Double)]): DataFrame = {
    harvest.toDF(beeIdColName, dayColName, pollenIdColName, miligramsHarvestedColName)
  }

  def toPollenDF(pollen: Seq[(Int, String, Int)]): DataFrame = {
    pollen.toDF(idColName, nameColName, sugarPerMgColName)
  }
}
