package com.gitlab.angelikabajor

package object measurements {

  private[measurements] val beeIdColName = "bee_id"
  private[measurements] val dayColName = "day"
  private[measurements] val pollenIdColName = "pollen_id"
  private[measurements] val miligramsHarvestedColName = "miligrams_harvested"

  private[measurements] val idColName = "id"
  private[measurements] val nameColName = "name"
  private[measurements] val sugarPerMgColName = "sugar_per_mg"

  private[measurements] val totalSugarColName = "total_sugar"
  private[measurements] val popularityColName = "popularity"

  private[measurements] def buildMaxValueQuery(viewName: String, colName: String): String = {
    s"SELECT * FROM $viewName WHERE $colName IN (SELECT MAX($colName) FROM $viewName)"
  }

  private[measurements] def buildMinValueQuery(viewName: String, colName: String): String = {
    s"SELECT * FROM $viewName WHERE $colName IN (SELECT MIN($colName) FROM $viewName)"
  }

}
