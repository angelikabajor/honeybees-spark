package com.gitlab.angelikabajor
package measurements

import SparkConfig.spark

import org.apache.spark.sql.DataFrame

object PollenMeasurements {

  def getMostContributedPollen(pollens: DataFrame, harvest: DataFrame): DataFrame = {
    val totalSugarByPollen = getTotalSugarByPollen(pollens, harvest)

    val viewName = "totalSugarByPollen"
    totalSugarByPollen.createOrReplaceTempView(viewName)
    spark.sql(buildMaxValueQuery(viewName, totalSugarColName))
  }

  def getMostPopularPollen(pollens: DataFrame, harvest: DataFrame): DataFrame = {
    val popularityPerPollen = getPopularityPerPollen(pollens, harvest)

    val viewName = "popularityPerPollen"
    popularityPerPollen.createOrReplaceTempView(viewName)
    spark.sql(buildMaxValueQuery(viewName, popularityColName))
  }

  private def getTotalSugarByPollen(pollens: DataFrame, harvest: DataFrame) = {
    val sugarPerDay = harvest
      .join(pollens, harvest(pollenIdColName) === pollens(idColName))
      .select(pollenIdColName, miligramsHarvestedColName, sugarPerMgColName)

    sugarPerDay
      .withColumn(totalSugarColName, sugarPerDay(miligramsHarvestedColName) * sugarPerDay(sugarPerMgColName))
      .select(pollenIdColName, totalSugarColName)
      .groupBy(pollenIdColName)
      .sum(totalSugarColName)
      .withColumnRenamed(s"SUM($totalSugarColName)", totalSugarColName)
  }

  private def getPopularityPerPollen[A](pollens: DataFrame, harvest: DataFrame) = {
    harvest
      .join(pollens, harvest(pollenIdColName) === pollens(idColName))
      .groupBy(pollenIdColName)
      .count()
      .withColumnRenamed("count", popularityColName)
  }

}
