package com.gitlab.angelikabajor
package measurements

import SparkConfig.spark

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DecimalType

object BeeMeasurements {

  def getBeesEffectiveness(pollens: DataFrame, harvest: DataFrame): DataFrame = {
    val sugarPerBee = harvest
      .join(pollens, harvest(pollenIdColName) === pollens(idColName))
      .select(beeIdColName, miligramsHarvestedColName, sugarPerMgColName)

    sugarPerBee
      .withColumn(totalSugarColName, sugarPerBee(miligramsHarvestedColName) * sugarPerBee(sugarPerMgColName))
      .select(beeIdColName, totalSugarColName)
      .groupBy(beeIdColName)
      .avg(totalSugarColName)
      .withColumnRenamed(s"AVG($totalSugarColName)", totalSugarColName)
      .withColumn(totalSugarColName, col(totalSugarColName).cast(DecimalType(10, 2)))
  }

  def getMostEffectiveBees(beesEffectiveness: DataFrame): DataFrame = {
    val viewName = "beesEffectiveness"
    beesEffectiveness.createOrReplaceTempView(viewName)
    spark.sql(buildMaxValueQuery(viewName, totalSugarColName))
  }

  def getLeastEffectiveBees(beesEffectiveness: DataFrame): DataFrame = {
    val viewName = "beesEffectiveness"
    beesEffectiveness.createOrReplaceTempView(viewName)
    spark.sql(buildMinValueQuery(viewName, totalSugarColName))
  }

}
