package com.gitlab.angelikabajor
package measurements

import SparkConfig.spark

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DecimalType

object DayMeasurements {

  def getSugarPerDay(pollens: DataFrame, harvest: DataFrame): DataFrame = {
    val sugarPerDay = harvest
      .join(pollens, harvest(pollenIdColName) === pollens(idColName))
      .select(dayColName, miligramsHarvestedColName, sugarPerMgColName)

    sugarPerDay
      .withColumn(totalSugarColName, sugarPerDay(miligramsHarvestedColName) * sugarPerDay(sugarPerMgColName))
      .select(dayColName, totalSugarColName)
      .groupBy(dayColName)
      .sum(totalSugarColName)
      .withColumnRenamed(s"SUM($totalSugarColName)", totalSugarColName)
      .withColumn(totalSugarColName, col(totalSugarColName).cast(DecimalType(10, 2)))
  }

  def getMostEfficientDays(sugarByDay: DataFrame): DataFrame = {
    val viewName = "sugarByDay"
    sugarByDay.createOrReplaceTempView(viewName)
    spark.sql(buildMaxValueQuery(viewName, totalSugarColName))
  }

  def getLeastEfficientDays(sugarByDay: DataFrame): DataFrame = {
    val viewName = "sugarByDay"
    sugarByDay.createOrReplaceTempView(viewName)
    spark.sql(buildMinValueQuery(viewName, totalSugarColName))
  }

}
