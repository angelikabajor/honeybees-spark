package com.gitlab.angelikabajor

import org.apache.spark.sql.SparkSession

object SparkConfig {

  val spark: SparkSession = SparkSession.builder()
    .master("local[*]")
    .appName("Honeybees Spark")
    .getOrCreate()

}
