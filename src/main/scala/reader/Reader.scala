package com.gitlab.angelikabajor
package reader

import org.apache.spark.sql.{DataFrame, SparkSession}

object Reader {

  def readCsv(spark: SparkSession, filename: String): DataFrame = {
    val output: DataFrame = spark
      .read
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(getClass.getClassLoader.getResource(filename).getPath)

    var outputWithTrimmedHeaders = output
    for (col <- output.columns) {
      outputWithTrimmedHeaders = outputWithTrimmedHeaders.withColumnRenamed(col, col.trim)
    }
    outputWithTrimmedHeaders
  }
}
