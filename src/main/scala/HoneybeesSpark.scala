package com.gitlab.angelikabajor

import SparkConfig.spark
import measurements.{BeeMeasurements, DayMeasurements, PollenMeasurements}
import reader.Reader.readCsv

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql._
import org.apache.spark.sql.functions.col

object HoneybeesSpark extends App {

  Logger.getLogger("org.apache.spark").setLevel(Level.WARN)

  private val pollensFilename = "pollens.csv"
  private val harvestFilename = "harvest.csv"

  private val harvest: DataFrame = readCsv(spark, harvestFilename)
  private val pollens: DataFrame = readCsv(spark, pollensFilename)

  private val mostContributedPollen: DataFrame = PollenMeasurements.getMostContributedPollen(pollens, harvest)
  private val mostPopularPollen: DataFrame = PollenMeasurements.getMostPopularPollen(pollens, harvest)
  private val sugarByDay: DataFrame = DayMeasurements.getSugarPerDay(pollens, harvest)
  private val mostEfficientDays: DataFrame = DayMeasurements.getMostEfficientDays(sugarByDay)
  private val leastEfficientDays: DataFrame = DayMeasurements.getLeastEfficientDays(sugarByDay)
  private val beesEffectiveness: DataFrame = BeeMeasurements.getBeesEffectiveness(pollens, harvest)
  private val mostEffectiveBees: DataFrame = BeeMeasurements.getMostEffectiveBees(beesEffectiveness)
  private val leastEffectiveBees: DataFrame = BeeMeasurements.getLeastEffectiveBees(beesEffectiveness)

  println("1. Which pollen contributed the most sugar in total?")
  mostContributedPollen.show()
  println("2. Which pollen was most popular among the bees?")
  mostPopularPollen.show()

  println("3. Which day was the best day for harvesting? Which one was the worst? (A chart or table that shows total sugar per day would be great.")
  println("\nMost efficient day:")
  mostEfficientDays.show()
  println("\nLeast efficient day:")
  leastEfficientDays.show()
  println("\nSugar by day:")
  sugarByDay.orderBy(col("day")).show()

  println("4. Which bee was most effective (**udarnik**)? Which one was the least? Effectiveness is measured as average sugar harvested per each working day. Getting a table with each bee relative effectiveness is a plus.")
  println("\nMost effective bee:")
  mostEffectiveBees.show()
  println("\nLeast effective bee:")
  leastEffectiveBees.show()
  println("\nBees effectiveness:")
  beesEffectiveness.orderBy(col("bee_id")).show()

}